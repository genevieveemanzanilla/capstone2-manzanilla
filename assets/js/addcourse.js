let formSubmit = document.querySelector("#createCourse")

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")

let nameSpan = name.nextElementSibling
let priceSpan = price.nextElementSibling
let descriptionSpan = description.nextElementSibling 

console.log(nameSpan)
console.log(priceSpan)
console.log(descriptionSpan)


console.log(formSubmit)

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault();

	console.log("I trigerred the submit button")

	let inputErrs = [
		displayErrMsg(name,nameSpan),
		displayErrMsg(price,priceSpan),
		displayErrMsg(description, descriptionSpan)
	]

	console.log(inputErrs)

	if(!inputErrs.includes(true)){
		console.log("yay")
		let courseName = document.querySelector("#courseName").value
		let description = document.querySelector("#courseDescription").value
		let price = document.querySelector("#coursePrice").value

		let token = localStorage.getItem("token")
		
		fetch('https://agile-ridge-32567.herokuapp.com/api/courses/', {
			method: 'POST',
			headers: { 
				'Content-Type':'application/json',
				'Authorization':`Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				window.location.replace('./courses.html')
			} else {
				alert("Course creation failed. Something went wrong.")
			}
		})
	}	

})

function displayErrMsg(inputField,messageSpan){
	if (inputField.value.length === 0) {
		messageSpan.classList.remove("d-none");
		return true;
	} else {
		messageSpan.classList.add("d-none");
		return false;
	}
}