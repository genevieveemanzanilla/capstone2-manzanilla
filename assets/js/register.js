let registerForm = document.querySelector("#registerUser")
let password = document.querySelector("#password1");
let confirmPassword = document.querySelector("#password2")
let regBtn = document.querySelector("#reg-btn");
let firstName = document.querySelector("#firstName")
let lastName = document.querySelector("#lastName")
let mobileNo = document.querySelector("#mobileNumber")
let email = document.querySelector("#userEmail")
let password1 = document.querySelector("#password1")
let password2 = document.querySelector("#password2")
let passwordSpan = document.querySelector("#passwordSpan")

let firstNameSpan = firstName.nextElementSibling
let lastNameSpan = lastName.nextElementSibling
let mobileNoSpan = mobileNo.nextElementSibling
let emailSpan = email.nextElementSibling

console.log(passwordSpan)

registerForm.addEventListener("submit", (e) => {

	e.preventDefault()

	console.log("I triggered the submit event")

	let inputErrs = [
		displayErrMsg(firstName,firstNameSpan),
		displayErrMsg(lastName,lastNameSpan),
		displayErrMsg(email,emailSpan),
		displayNumErr(mobileNumber,mobileNoSpan)
	]

	console.log(inputErrs)

	if (!inputErrs.includes(true)) {

		let firstName = document.querySelector("#firstName").value
		let lastName = document.querySelector("#lastName").value
		let mobileNo = document.querySelector("#mobileNumber").value
		let email = document.querySelector("#userEmail").value
		let password1 = document.querySelector("#password1").value


		fetch('https://agile-ridge-32567.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
			// "{email: email}"
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if (data === false) {

				fetch('https://agile-ridge-32567.herokuapp.com/api/users', {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if(data === true){
						alert("registered successfully")

						//redirect to login page
						window.location.replace("./login.html")
					} else {
						// error in creating registration
						alert("something went wrong")
					}

				})

			}

		})
	}

})

password.addEventListener("keyup",() => {
	toggleDisableRegBtn();
})

confirmPassword.addEventListener("keyup",()=> {
	toggleDisableRegBtn();
})

function isValidPassword(passVal,confirmPassVal){
	return passVal === confirmPassVal && passVal.length > 7 ? true : false;
}

function toggleDisableRegBtn(){
	let passVal = password.value
	let confirmPassVal = confirmPassword.value

	if (isValidPassword(passVal,confirmPassVal)) {
		passwordSpan.classList.add("d-none");
		regBtn.disabled = false;
	} else {
		passwordSpan.classList.remove("d-none");
		regBtn.disabled = true;
	}
}

function displayErrMsg(inputField,messageSpan){
	if (inputField.value.length === 0) {
		messageSpan.classList.remove("d-none");
		return true;
	} else {
		messageSpan.classList.add("d-none");
		return false;
	}
}

function displayNumErr(inputField,messageSpan){
	if (inputField.value.length < 11) {
		messageSpan.classList.remove("d-none");
		return true;
	} else {
		messageSpan.classList.add("d-none");
		return false;
	}
}