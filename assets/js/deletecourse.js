let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
console.log(courseId);
let token = localStorage.getItem('token');

fetch(`https://agile-ridge-32567.herokuapp.com/api/courses/${courseId}`,{

	method : 'PUT',
	headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})


})
.then(res => res.json())
.then(data => {

	if(data === true){
				alert('Course has been archived')
				window.location.replace('./courses.html')
			} else {
				alert("Something went wrong")
			}
		

	
	
})