let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')

let token = localStorage.getItem('token')
let adminUser = localStorage.getItem('isAdmin')

let courseName = document.querySelector('#courseName')
let courseDesc = document.querySelector('#courseDesc')
let coursePrice = document.querySelector('#coursePrice')
let enrollContainer = document.querySelector('#enrollContainer')
let enrolleeContainer = document.querySelector("#enrolleeContainer")
let enrolleeHeader = document.querySelector("#enrolleeHeader")


fetch(`https://agile-ridge-32567.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	let enrollees = data.enrollees

	if(adminUser === "true") {

		if(enrollees.length < 1){
			enrollContainer.innerHTML = "No Enrollees Available"
		} else {

			enrollContainer.innerHTML = `<h4 class="card-title mt-5">Enrollees</h4>`
			
			enrollees.forEach(enrollee => {
				fetch('https://agile-ridge-32567.herokuapp.com/api/users/')
				.then(res => res.json())
				.then(users => {
					
					users.forEach(user => {

						if(enrollee.userId === user._id){

						firstName = user.firstName.charAt(0).toUpperCase() + user.firstName.slice(1)
						lastName = user.lastName.charAt(0).toUpperCase() + user.lastName.slice(1)

						enrolleeContainer.innerHTML += `
							<div class="card-container col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">${firstName} ${lastName}</h5>
										<h6 class="card-subtitle mb-2">Enroll date:</h6>
										<p class="card-text text-center">${Date(enrollee.enrolledOn).toLocaleString()}</p>
									</div>
								</div>
							</div>								

							`

						}

					})
				})
			})

		}
	//admin condition	
	} else {

		enrollContainer.innerHTML = `<button id="enrollButton" class="btn col-6 btn-primary">Enroll</button>`

		document.querySelector("#enrollButton").addEventListener("click", () => {

			let userId = localStorage.getItem('id');
			enrolled = enrollees.map(enrollee => {	
				return enrollee.userId === userId ? true: false;	
			})			
			

			if(!enrolled.includes(true)){

				fetch('https://agile-ridge-32567.herokuapp.com/api/users/enroll', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId
					})
				})
				.then(res=>res.json())
				.then(data => {
					if(data === true){
						alert('Thank you for enrolling to the course.')
						window.location.replace('./courses.html')
					} else {
						alert("Login first to enroll")
						window.location.replace('./login.html')
					}
				}) // enroll route
			} else {
				alert("You are already enrolled to this class")
				window.location.replace('./courses.html')				
			}

		}) //end of click event

	} // end of not admin condition

})

