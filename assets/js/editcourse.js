let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');

let formSubmit = document.querySelector("#editCourse")
let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")

let nameSpan = name.nextElementSibling
let priceSpan = price.nextElementSibling
let descriptionSpan = description.nextElementSibling 

fetch(`https://agile-ridge-32567.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then( data => {
	
	name.value = data.name
	description.value = data.description
	price.value = data.price

	name.placeholder = ("Course Name")
	description.placeholder = ("Description")
	price.placeholder = ("Price")
})

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	console.log("I trigerred the save event")

	let inputErrs = [
		displayErrMsg(name,nameSpan),
		displayErrMsg(price,priceSpan),
		displayErrMsg(description, descriptionSpan)
	]

	console.log(inputErrs)

	if(!inputErrs.includes(true)) {

		let name = document.querySelector("#courseName").value
		let description = document.querySelector("#courseDescription").value
		let price = document.querySelector("#coursePrice").value

		fetch("https://agile-ridge-32567.herokuapp.com/api/courses", {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				id: courseId,
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {
				window.location.replace('./courses.html')
			} else {
				alert("Course edit failed: Something Went Wrong")
			}
		})
	}
})

function displayErrMsg(inputField,messageSpan){
	if (inputField.value.length === 0) {
		messageSpan.classList.remove("d-none");
		return true;
	} else {
		messageSpan.classList.add("d-none");
		return false;
	}
}