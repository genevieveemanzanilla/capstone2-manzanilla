let userId = localStorage.getItem('id');
let token = localStorage.getItem('token');
console.log(userId)

fetch('https://agile-ridge-32567.herokuapp.com/api/users/details', {
headers: {
	Authorization: `Bearer ${token}` 
		}
})
.then(res => res.json())
.then(data =>{
	firstName.value = data.firstName.charAt(0).toUpperCase() + data.firstName.slice(1)
	lastName.value = data.lastName.charAt(0).toUpperCase() + data.lastName.slice(1)
	mobileNo.value = data.mobileNo
	email.value = data.email
	firstName.placeholder = ("First Name")
	lastName.placeholder = ("Last Name")
	mobileNo.placeholder = ("Mobile Number")
	email.placeholder = ("Email Address")
})

let formSubmit = document.querySelector("#editProfile")



formSubmit.addEventListener("submit", (e) => {

	let firstName = document.querySelector("#firstName")
	let lastName = document.querySelector("#lastName")
	let mobileNo = document.querySelector("#mobileNo")
	let email = document.querySelector("#email")

	let firstNameSpan = firstName.nextElementSibling
	let lastNameSpan = lastName.nextElementSibling
	let mobileNoSpan = mobileNo.nextElementSibling
	let emailSpan = email.nextElementSibling
	
	e.preventDefault()

	let inputErrs = [
		displayErrMsg(firstName,firstNameSpan),
		displayErrMsg(lastName,lastNameSpan),
		displayErrMsg(email,emailSpan),
		displayNumErr(mobileNo,mobileNoSpan)
	]

	console.log(inputErrs)	

	if (!inputErrs.includes(true)) {

		firstName = document.querySelector("#firstName").value
		lastName = document.querySelector("#lastName").value
		mobileNo = document.querySelector("#mobileNo").value
		email = document.querySelector("#email").value

		fetch("https://agile-ridge-32567.herokuapp.com/api/users/",
		{
			method: 'PUT',
			headers: {
				'Content-Type':'application/json',
				'Authorization':`Bearer ${token}` 
			},
			body: JSON.stringify({
				id: userId,
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {
				alert("Profile updated!")
				window.location.replace('./profile.html')
			} else {
				alert("Profile edit failed: Something Went Wrong")
			}
		})
	}

	

})

function displayErrMsg(inputField,messageSpan){
	if (inputField.value.length === 0) {
		messageSpan.classList.remove("d-none");
		return true;
	} else {
		messageSpan.classList.add("d-none");
		return false;
	}
}

function displayNumErr(inputField,messageSpan){
	if (inputField.value.length < 11) {
		messageSpan.classList.remove("d-none");
		return true;
	} else {
		messageSpan.classList.add("d-none");
		return false;
	}
}