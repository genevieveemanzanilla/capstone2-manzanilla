let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")
let courseHeader = document.querySelector("#courseHeader")
let cardFooter;

if(adminUser === false || adminUser === null){

	addButton.innerHTML = null

} else {
	addButton.innerHTML = `

		<div class="col-md-3 offset-md-9">
			<a href="./addcourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>


	`
}

if(adminUser === true){

	fetch('https://agile-ridge-32567.herokuapp.com/api/courses/all')
	.then(res => res.json())
	.then(data => {

		console.log(data);

		
		let courseData;

		if(data.length < 1) {
			courseData = "No courses available";
		} else {

			courseHeader.innerHTML = 
			`
			<div class="text-center py-5" >
				<h2>Courses</h4>
			<div>
			`

			courseData = data.map(course => {

				

				if(course.isActive == true) {
					cardFooter =
					`
						<a href="./editcourse.html?courseId=${course._id}" class="btn my-1 col-lg-3 editButton">
							Edit
						</a>
						<a href="./course.html?courseId=${course._id}" class="btn my-1 col-lg-3 viewButton">
							View
						</a>
						<a href="./deletecourse.html?courseId=${course._id}" class="btn btn-danger my-1 col-lg-4 archiveButton">
							Archive
						</a>
					`
				} else {
					cardFooter = 
					`
						<a href="./editcourse.html?courseId=${course._id}" class="btn my-1 col-lg-3 editButton">
							Edit
						</a>
						<a href="./course.html?courseId=${course._id}" class="btn my-1 col-lg-3 viewButton">
							View
						</a>
						<a href="./activatecourse.html?courseId=${course._id}" class="btn btn-success my-1 col-lg-4 activateButton">
							Activate
						</a>
					`
				}

				return(
					`
						<div class="card-container col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-center">
										${course.description}
									</p>
									<p class="card-text text-right">
										₱ ${course.price}
									</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				)

			// since the collection is an array, we can use the join method to indicate the separator for each element
			}).join("")

		}

		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData;

	})

} else {

	fetch('https://agile-ridge-32567.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {

		console.log(data);

		
		let courseData;

		if(data.length < 1) {
			courseData = "No courses available";
		} else {

			courseHeader.innerHTML = `<h2 class="text-center py-5">Courses</h4>`

			courseData = data.map(course => {


					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" class="btn col-md-12 col-lg-6 selectButton">
							Select Course
						</a>
					`
				

				return(
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-center">
										${course.description}
									</p>
									<p class="card-text text-center">
										₱ ${course.price}
									</p>
								</div>
								<div class="card-footer text-center">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				)

			// since the collection is an array, we can use the join method to indicate the separator for each element
			}).join("")

		}

		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData;

	})	

}



