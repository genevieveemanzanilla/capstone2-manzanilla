let userId = localStorage.getItem('id');
let token = localStorage.getItem('token');
let adminUser = localStorage.getItem("isAdmin")

let enrollContainer = document.querySelector('#enrollContainer')

fetch('https://agile-ridge-32567.herokuapp.com/api/users/details', {
headers: {
	Authorization: `Bearer ${token}` 
		}
})
.then(res => res.json())
.then(data => {

	if(adminUser === "true"){


			profileContainer.innerHTML = `<h1 class="text-center">No Profile Available</h1>`


	} else {



		firstName = data.firstName.charAt(0).toUpperCase() + data.firstName.slice(1)
		lastName = data.lastName.charAt(0).toUpperCase() + data.lastName.slice(1)
			
		profileContainer.innerHTML =
		`
			<div class="col-md-12">
				<section class="jumbotron text-center my-4">
					<div class="profile-body">
						<h2 class = "my-4">${firstName} ${lastName}</h2>
						<p class="card-text text-center">Mobile number: ${data.mobileNo}</p>
						<p class="card-text text-center">Email address: ${data.email}</p>
					</div>				
					<div class="profile-footer my-3 col-md-4 offset-md-8">
						<a href="./editprofile.html" class="btn editButton">
						Edit Profile
						</a>
					</div>
					
					<div id="enrollmentsContainer"></div>				
				</section>
			</div>
		`

		console.log(data.enrollments.length)


		let enrollmentCard = document.querySelector("#enrollmentsContainer")
		
		if (data.enrollments.length > 0){
			enrollContainer.innerHTML = `<h4 class="card-title mt-5">Enrolled Courses</h4>`
		}

		data.enrollments.map(enrollments => {

			let courseId = enrollments.courseId
			console.log(courseId)
			fetch(`https://agile-ridge-32567.herokuapp.com/api/courses/${courseId}`).then(res=>res.json()).then(data => {

				enrollmentCard.innerHTML += `

						<div class="card my-3">
							<div class="card-body">
							    <h5 class="card-title">${data.name}</h5>
							    <h6 class="card-subtitle mb-2">Enrolled On: ${Date(enrollments.enrolledOn).toLocaleString()}</h6>
							    <h6 class="card-subtitle mb-2">Status:${enrollments.status}</h6>
							</div>
						</div>

						`

			})			
		})

	}
})
